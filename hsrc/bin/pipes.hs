{-# LANGUAGE BangPatterns #-}

--------------------------------------------------------------------------------
-- |For each gzip file given on the command line, parse each line as either a
-- Even or Odd Token, depending on the line length, and then do one of two
-- things:
--
-- 1) sink1: Print out each Token found.  8Mb max RES memory when given 2 14Mb
--    gzip files.
-- 2) sink2: Count the number of occurrences for Even and Odd Tokens, and print
--    the results.  300Mb max RES memory when given 2 14Mb gzip files.
--
-- Why is the GHC runtime using almost 40x as much memory for sink2?  The
-- difference only grows larger when I pass more gzip files as arguments.  Are
-- thunks being added to the heap, or maybe heap objects that exist in both
-- sinks are not being GC'd in sink2?
module Main where


--------------------------------------------------------------------------------
import Data.ByteString.Char8 ( ByteString )
import qualified Data.ByteString.Char8 as BS
import System.IO ( IOMode(ReadMode)
                 , withBinaryFile
                 , Handle
                 )
import Pipes
import Pipes.ByteString ( fromHandle )
import qualified Pipes.ByteString as PBS
import Pipes.Zlib ( decompress
                  , defaultWindowBits
                  )


--------------------------------------------------------------------------------
main :: IO ()
main = withBinaryFile f ReadMode action
  where f = "/tmp/tcmsdb/src/6e026cd556e8b798c227c1b295ea8159.gz"
        action handle = runEffect $
                        dec (fromHandle handle) >-> PBS.lines >-> echo
        dec = decompress defaultWindowBits




echo :: Consumer ByteString IO ()
echo = do
  str <- await
  lift $ BS.putStrLn str
  echo
