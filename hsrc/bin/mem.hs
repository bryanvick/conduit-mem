{-# LANGUAGE BangPatterns #-}

--------------------------------------------------------------------------------
-- |For each gzip file given on the command line, parse each line as either a
-- Even or Odd Token, depending on the line length, and then do one of two
-- things:
--
-- 1) sink1: Print out each Token found.  8Mb max RES memory when given 2 14Mb
--    gzip files.
-- 2) sink2: Count the number of occurrences for Even and Odd Tokens, and print
--    the results.  300Mb max RES memory when given 2 14Mb gzip files.
--
-- Why is the GHC runtime using almost 40x as much memory for sink2?  The
-- difference only grows larger when I pass more gzip files as arguments.  Are
-- thunks being added to the heap, or maybe heap objects that exist in both
-- sinks are not being GC'd in sink2?
module Main where


--------------------------------------------------------------------------------
import Control.Monad.Trans.Resource ( runResourceT )
import Control.Monad.IO.Class ( MonadIO, liftIO )
import qualified Data.ByteString.Char8 as BS
import Data.Conduit ( Sink, Conduit, (=$), ($$), await, yield, awaitForever )
import qualified Data.Conduit.List as CL
import qualified Data.Conduit.Binary as CB
import qualified Data.Conduit.Zlib as Zlib
import System.Environment ( getArgs )


--------------------------------------------------------------------------------
data Args      = Help | GzipFiles [GzipFilePath]
data Error     = NotAGzip FilePath deriving (Show)
data Token     = Even | Odd deriving (Show, Eq)
type Line      = BS.ByteString
type SinkState = (EvenCount, OddCount)
type EvenCount = Integer
type OddCount  = Integer
newtype GzipFilePath = GzipFilePath FilePath


--------------------------------------------------------------------------------
main :: IO ()
main = do
  cmdLine <- getArgs
  let tryArgs = parseArgs cmdLine
  case tryArgs of
    (Left e)                  -> putStrLn (show e)
    (Right Help)              -> putStrLn help
    (Right (GzipFiles files)) -> CL.sourceList files $$ CL.mapM_ action
  where help = "USAGE: mem GZIPFILE [GZIPFILE..]"


-------------------------------------------------------------------------------
action :: GzipFilePath -> IO ()
action (GzipFilePath filePath) = do
  result <- runResourceT $  CB.sourceFile filePath
                         $$ Zlib.ungzip
                         =$ CB.lines
                         =$ token
                         =$ sink
  putStrLn $ show result
  where sink  = sink3
        -- sink = sink2 (0,0)
        -- sink = CL.mapM_ (liftIO . putStrLn . show)
        -- sink = sink1


--------------------------------------------------------------------------------
token :: (Monad m) => Conduit Line m Token
token = awaitForever tokenize
  where tokenize line | even $ BS.length line = yield Even
                      | otherwise             = yield Odd


--------------------------------------------------------------------------------
-- |Print each Token.
--
-- This has a max resident set size of about 8Mb when I pass two 120Mb files
-- as arguments.
sink1 :: MonadIO m => Sink Token m ()
sink1 = awaitForever (liftIO . putStrLn . show)


--------------------------------------------------------------------------------
-- |Count Even and Odd Tokens.
--
-- This has a max resident set size of about 300Mb when I pass two 120Mb files
-- as arguments.
--
-- Note that the SinkState tuple is strict, both in the tuple constructor, and
-- its two Integers.  As I understand it, this should keep a bunch of addition
-- thunks from building up on the heap.
sink2 :: (Monad m) => SinkState -> Sink Token m SinkState
sink2 state@(!evenCount, !oddCount) = do
  maybeToken <- await
  case maybeToken of
    Nothing     -> return state
    (Just Even) -> sink2 (evenCount + 1, oddCount    )
    (Just Odd ) -> sink2 (evenCount    , oddCount + 1)


--------------------------------------------------------------------------------
-- |sink2 written using fold.
--
-- This gives us constant memory, unlike sink2.
sink3 :: (Monad m) => Sink Token m SinkState
sink3 = CL.fold add (0, 0)
  where add (!x, !y) Even = (x + 1, y)
        add (!x, !y) Odd  = (x, y + 1)


--------------------------------------------------------------------------------
parseArgs :: [String] -> Either Error Args
parseArgs []            = Right Help
parseArgs ("-h":[])     = Right Help
parseArgs ("--help":[]) = Right Help
parseArgs as            = fmap GzipFiles $ sequence $ fmap parse as
  where parse a = let (_, ext) = break ((==) '.') a
                  in case ext of
                    ".gz" -> Right $ GzipFilePath a
                    _     -> Left  $ NotAGzip     a
